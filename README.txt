Menu Builder
------------------------
This module provides an easier way to build menu items in bulk without a bunch of
clicking and paging in Drupal. All you need to do is create a formatted file similar
to the one below. Parents are listed without dashes, and the children and children
level are listed with the appropriate number of dashes.

One dash makes it a child of the previous item. Two dashes makes it a child to that
child. The example should be pretty easy to follow. Specify the link by inserting a
pipe character and specifying either the drupal page or the full URL to the destination.

Once created, copy and paste the file into the builder and you'll be set. You can even
specify the menu to import to or have a new one created.

Example
------------------------------
Content|<front>
-Advanced Search|search
--Test|nested-child-example
- Weekly Newsletter/newsletter
Tools|tools
-My Stuff |my-stuff
-Manage Alerts and Feeds|feeds
-My Profile|user
About|about
-Out Staff|about/staff
-Our Products|about/products
-Contact Us|about/contact-us
-Feedback|habout/feedback
Social|<front>
-Read our blog|http://www.blog.typepad.com
-Follow us on Twitter|http://twitter.com
-Like us on Facebook|http://www.facebook.com
-Follow us on Tumbler|http://www.tumblr.com
-Join us on Google+|https://plus.google.com